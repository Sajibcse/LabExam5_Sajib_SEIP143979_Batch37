<?php
$stardate = new DateTime("1981-11-03");
$expecteddate = new DateTime("2013-09-04");

$interval = $stardate->diff($expecteddate);
echo "Difference : " . $interval->y . " years, " . $interval->m." months, ".$interval->d." days ";
?>